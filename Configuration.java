public class Configuration {
    private Map<Action.Key, Pair<GaAction, List<Param>>> gaActions = new HashMap<>();
    private Map<Action.Key, Pair<BiAction, List<Param>>> biActions = new HashMap<>();

    public void addGaAction(GaAction action, List<Param> params) {
        config.getGaActions().put(action.key, new Pair<>(action, params));
    }

    public void addBiAction(BiAction action, List<Param> params) {
        config.getBiActions().put(action.key, new Pair<>(action, params));
    }

    public Map<Action.Key, Pair<GaAction, List<Param>>> getGaActions() {
        return gaActions;
    }

    public Map<Action.Key, Pair<BiAction, List<Param>>> getBiActions() {
        return biActions;
    }

    /**
     * Example of what the configuration would look like.
     * This creates a 'receipt listing viewed' action for both GA and BI
     */
    public static Configuration createSampleConfig() {
        Configuration config = new Configuration();

        config.addGaAction(new GaAction(
                Action.Key.ReceiptListingViewed, "list retreived"),
                Arrays.asList(
                        Param.CurrentFilter,
                        Param.CurrentSort,
                        Param.NumberOfReceipts));

        config.addBiAction(new BiAction(
                Action.Key.ReceiptListingViewed, 1720043, "ReceiptDetailViewed", "User viewed receipt detail"),
                Arrays.asList(
                        Param.CurrentFilter,
                        Param.CurrentSort,
                        Param.NumberOfReceipts,
                        Param.ActionSource,
                        Param.NumOfFilteredReceipts));

        return config;
    }
}
