import android.support.v4.app.Fragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * @author David Whitman on Jun 01, 2016.
 */
public class Logger {
    Configuration config;
    @Inject BiLogger biLogger;
    @Inject GaLogger gaLogger;

    public Logger() {
        // Initialize with a sample configuration
        initialize(Configuration.createSampleConfig());
    }

    public void initialize(Configuration configuration) {
        config = configuration;
    }

    /**
     * Log an event
     */
    public void log(Loggable loggable, LoggingEvent event) {
        Action.Key action = event.getAction();

        // If BI logs this action, then get the required params from the event and log it
        if (config.getBiActions().containsKey(action)) {
            BiAction biAction = config.getBiActions().get(action).first;
            List<Param> requiredParams = config.getBiActions().get(action).second;
            Map<Param, Object> foundParams = new HashMap<>();
            List<Param> missingParams = new ArrayList<>();
            StringBuilder missingParamsMessage = new StringBuilder("Missing from " + action.name() + " in BI: ");

            // Find all params that are needed for this BI action
            for (Param param : requiredParams) {
                if (event.getParams().keySet().contains(param)) {
                    foundParams.put(param, event.getParams().get(param));
                } else {
                    missingParams.add(param);
                    missingParamsMessage.append(param).append(" ");
                }
            }

            if (!missingParams.isEmpty()) {
                throw new IllegalArgumentException(missingParamsMessage.toString());
            }

            biLogger.log(biAction, foundParams);
        }


        // If GA logs this action, then get the required params from the event and log it
        if (config.getGaActions().containsKey(action)) {
            GaAction gaAction = config.getGaActions().get(action).first;
            List<Param> requiredParams = config.getGaActions().get(action).second;
            Map<Param, Object> foundParams = new HashMap<>();
            List<Param> missingParams = new ArrayList<>();
            StringBuilder missingParamsMessage = new StringBuilder("Missing from " + action.name() + " in GA: ");

            // Find all params that are needed for this BI action
            for (Param param : requiredParams) {
                if (event.getParams().keySet().contains(param)) {
                    foundParams.put(param, event.getParams().get(param));
                } else {
                    missingParams.add(param);
                    missingParamsMessage.append(param).append(" ");
                }
            }

            if (!missingParams.isEmpty()) {
                throw new IllegalArgumentException(missingParamsMessage.toString());
            }

            gaLogger.log(gaAction, foundParams);
        }
    }
}
