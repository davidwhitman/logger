interface Action {
    enum Key {
        ReceiptListingViewed,
        ReceiptDetailViewed
        // todo: add all the rest of the possible actions in both ga and bi
    }
}

public static class GaAction implements Action {
    private final Key key;
    private final String message;

    public GaAction(Action.Key key, String message) {
        this.key = key;
        this.message = message;
    }

    public Key getKey() {
      return key;
    }

    public void setKey(Key value) {
      key = value;
    }

    public String getMessage() {
      return message;
    }

    public void setMessage(String value) {
      message = value;
    }
}

public static class BiAction implements Action {
    private final Key key;
    private final int id;
    private final String name;
    private final String description;

    public BiAction(Action.Key key, int id, String name, String description) {
        this.key = key;
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public Key getKey() {
      return key;
    }

    public void setKey(Key value) {
      key = value;
    }

    public int getId() {
      return id;
    }

    public void setId(int value) {
      id = value;
    }

    public String getName() {
      return name;
    }

    public void setName(String value) {
      name = value;
    }

    public String getDescription() {
      return description;
    }

    public void setDescription(String value) {
      description = value;
    }
}
