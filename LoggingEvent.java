public class LoggingEvent {
    private Action.Key action;
    private Map<Param, Object> params;

    public LoggingEvent(Action.Key action, Map<Param, Object> params) {
        this.action = action;
        this.params = params;
    }

    public Action.Key getAction() {
        return action;
    }

    public Map<Param, Object> getParams() {
        return params;
    }
}
