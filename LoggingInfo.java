public class LoggingInfo {
    private String gaScreenName;
    private String biScreenName;

    public String getGaScreenName() {
        return gaScreenName;
    }

    public void setGaScreenName(String gaScreenName) {
        this.gaScreenName = gaScreenName;
    }

    public String getBiScreenName() {
        return biScreenName;
    }

    public void setBiScreenName(String biScreenName) {
        this.biScreenName = biScreenName;
    }
}
