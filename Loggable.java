public interface Loggable {
    LoggingInfo getLoggingInfo();
}
