/** Sample class with a single logging call **/
public class ViewExample extends Fragment implements Logger.Loggable {
    private Logger.LoggingInfo loggingInfo;
    private Logger logger = new Logger();

    @Override
    public Logger.LoggingInfo getLoggingInfo() {
        if (loggingInfo == null) {
            loggingInfo = new Logger.LoggingInfo();
            loggingInfo.setBiScreenName("receipt listing screen name for bi");
            loggingInfo.setGaScreenName("receipt listing screen name for ga");
        }

        return loggingInfo;
    }

    /** Example of logging */
    public void logReceiptListingViewed() {
        Map<Logger.Configuration.Param, Object> params = new HashMap<>();
        params.put(Logger.Configuration.Param.CurrentFilter, "some CurrentFilter value");
        params.put(Logger.Configuration.Param.CurrentSort, "some CurrentSort value");
        params.put(Logger.Configuration.Param.NumberOfReceipts, "some NumberOfReceipts value");
        params.put(Logger.Configuration.Param.ActionSource, "some ActionSource value");
        params.put(Logger.Configuration.Param.NumOfFilteredReceipts, "some NumOfFilteredReceipts value");

        // Logs to both GA and BI
        logger.log(this, new Logger.LoggingEvent(Logger.Configuration.Action.Key.ReceiptListingViewed, params));
    }
}
